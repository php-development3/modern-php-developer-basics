<?php include_once 'nav.php'; ?>

Home Page

<?php
    function sumUp(int|float ...$nums)
    {
        return array_sum($nums);
    }
    
    echo sumUp(4, 5, 5, 6, 7, 9);
    
    $iAmGlobal = 10;
    const iAmConst = 20;
    
    function foo() {
//        echo $iAmGlobal;
        echo '<br>' . iAmConst;
        echo '<br>' . iAmConst;
    }
    
    foo();