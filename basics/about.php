<?php
    
    declare(strict_types=1);
    
    function getStatus(int|float $paymentStatus): string
    {
        return match($paymentStatus) {
            1 => "Success",
            2 => "Denied",
            default => "Unknown",
        };
    }
    
    function example(): bool
    {
        echo "Example called!";
        
        return true;
    }
    
    var_dump(true && example());
    
    var_dump(getStatus(2));
    