<?php
    
    declare(strict_types=1);
    
    function isLeap(int $year): bool
    {
        return (!($year % 4) && $year % 100) || !($year % 400);
    }
    
//    var_dump(isLeap(400));
    var_dump(!(1996 % 3));
