<?php
    
    declare(strict_types=1);
    
    function slices(string $series, int $size) : array
    {
        
        if ($size > strlen($series) || $size < 1) {
            return [];
        }
        
        $result = [];
        $itr = strlen($series) - $size;
        
        for ($x = 0; $x <= $itr; $x++) {
            $result[] = substr($series, $x, $size);
        }
        
        return $result;
    }
    
    print_r(slices("49142", 3));
    
    function slicesUdemy(string $series, int $size): array
    {
        $seriesLength = strlen($series);
        
        if ($size > strlen($series) || $size < 1) {
            return [];
        }
        
        $results = [];
        
        for ($i = 0; $i <= $seriesLength - $size; $i++) {
            $results[] = substr($series, $i, $size);
        }
        
        return $results;
    }