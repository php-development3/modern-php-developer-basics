<?php
    
    declare(strict_types=1);
    
    function getNewName(): string
    {
        return chr(rand(65, 90)) . chr(rand(65, 90)) . mt_rand(100, 999);
    }

    echo getNewName() . "<br>";
    
    function getNewNameUdemy(): string
    {
        $letters = range('A', 'Z');
        shuffle($letters);
        $number = mt_rand(100, 999);
        return "{$letters[0]}{$letters[1]}{$number}";
    }
    
    echo getNewNameUdemy();