<?php
    
    declare(strict_types=1);
    
    function isArmstrongNumber(int $number): bool
    {
        $numberStr = (string) $number;
        $numberLength = strlen($numberStr);
        
        $sum = 0;
        
        foreach (str_split($numberStr) as $char) {
            $sum += $char ** $numberLength;
        }
        
        return $sum === $number;
    }
    
    echo isArmstrongNumber(153);
    
    function isArmstrongNumberUdemy(int $number): bool
    {
        $digits = str_split((string) $number);
        $digitCount = count($digits);
        
        $digits = array_map(function ($digit) use ($digitCount) {
            return $digit ** $digitCount;
        }, $digits);
        
        return array_sum($digits) === $number;
    }