<?php
    
    namespace App;
    
    abstract class AbstractProduct
    {
        public function turnOn(): void
        {
            echo "Turning on product";
        }
        
        abstract public function setup();
    }