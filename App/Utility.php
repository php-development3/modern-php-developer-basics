<?php
    
    namespace App;
    
    class Utility
    {
        /**
         * Summary
         * @param array $array The array to output
         * @return void
         */
        public static function printArr(array $array): void
        {
            if (count($array) === 0)
            {
                throw new EmptyArrayException("Array is empty");
            }
            
            echo '<pre>';
            print_r($array);
            echo '</pre>';
        }
    }