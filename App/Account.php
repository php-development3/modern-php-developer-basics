<?php
    declare(strict_types=1);
    
    namespace App;
    
    class Account
    {
        public const INTEREST_RATE = 2;
        public static int $count = 0;
        public SocialMedia $socialMedia;
        public function __construct(private string $name, public float $balance)
        {
            $this->socialMedia = new SocialMedia();
            
            self::$count++;
        }
        
        public function deposit(float $amount): static
        {
            $this->balance += $amount;
            
            return $this;
        }
        
        public function getBalance()
        {
            return $this->balance;
        }
    }