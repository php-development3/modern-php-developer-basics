<?php
    
    namespace App;
    
    class ToasterPremium extends Toaster
    {
        protected int $slots;
        private int $duration;
        
        public function __construct(int $newDuration)
        {
            parent::__construct();
            $this->slots = 4;
            $this->duration = $newDuration;
        }
        
        public function toast(): void
        {
            parent::toast();
            echo " Toasting bread for {$this->duration} minutes.";
        }
    }