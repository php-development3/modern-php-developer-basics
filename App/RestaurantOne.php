<?php
    
    namespace App;
    
    class RestaurantOne implements RestaurantInterface
    {
        public function prepareFood(): void
        {
            echo "Preparing food";
        }
    }