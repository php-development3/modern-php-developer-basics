<?php

    declare(strict_types=1);
    
//    require_once 'App/Account.php';
//    require_once 'App/SocialMedia.php';
    
    spl_autoload_register(function ($class) {
       $formattedClass = str_replace("\\", "/", $class);
       $path = "{$formattedClass}.php";
       
       require_once $path;
    });
    
    use App\{Account, SocialMedia, Utility, ToasterPremium,
        RestaurantTwo, FoodApp, RestaurantOne, RestaurantInterface, EmptyArrayException};
    
    $myAccount = new Account('John', 10);
    $otherAccount = new Account("Jame", 15);
    
    var_dump($myAccount->balance);
    $myAccount?->deposit(50)->deposit(100);
    echo "<br>";
    var_dump($myAccount->balance);
    echo "<br>";
    var_dump(Account::INTEREST_RATE);
    echo "<br>";
    var_dump(Account::$count);
    echo "<br>";
    Utility::printArr([34, 5, 6]);
    echo "<br>";
    $myToaster = new ToasterPremium(10);
    $myToaster->toast();
    echo "<br>";
    echo "<br>";
    $restaurant = new FoodApp(new class implements RestaurantInterface
    {
        public function prepareFood()
        {
            echo "Anonym";
        }
    });
    
    echo "<br>";
    try {
        Utility::printArr([]);
    } catch (EmptyArrayException|InvalidArgumentException $e) {
        echo "Custom exception: {$e->getMessage()} <br>";
    }
    
    echo "Finished running script";
    
    echo "<br>";
    echo "<br>";
    
    $currentWeek = new \App\CurrentWeek();
    
    function foo(iterable $iterable): void
    {
        foreach ($iterable as $key => $item) {
            var_dump($key, $item);
            echo "<br>";
            echo "<br>";
        }
    }
    
    foo($currentWeek);
    
    